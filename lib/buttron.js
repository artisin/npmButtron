/*!
 * nib
 * Copyright (c) 2010 TJ Holowaychuk <tj@vision-media.ca>
 * MIT Licensed
 */

/**
 * Module dependencies.
 */

var path = require('path'),
    helper = require('./nodes/helper'),
    trans = require('./nodes/transition');

exports = module.exports = plugin;
exports.version = require(path.join(__dirname, '../package.json')).version;
exports.path = __dirname;

/**
 * Return the plugin callback for stylus.
 *
 * @return {Function}
 * @api public
 */

function plugin(opts) {
  var implicit = (opts && opts.implicit === false) ? false : true;
  return function(style){
    // debugger
    style.include(__dirname);
    // if (implicit) {
    //     style.import('buttron');
    // }
    // style.import('buttron')
    // style.define('getAttributes', helper.getAttributes);
    // style.define('isAllObjs', helper.isAllObjs);

    // style.define('buttronProcess', helper.buttronProcess);
    style.define('transExtractor', trans);

    style.define('debug', helper.debug);
    style.define('str', helper.str);

  };
}
