var stylus = require('stylus'),
    nodes  = stylus.nodes,
    utils  = stylus.utils,
    _      = require('lodash');




exports.str = function(arg, keys){
  console.log('||||||||||||===========================>')
  console.log(arg)
  console.log(arg.toString())
  if (keys) {
    _.forEach(arg.vals, function(n, key) {
      console.log('Key ----> ' + key)
      console.log(n.toString())
      // debugger
      // console.log(key.vals.toBlock());
    });
  }
};




exports.getAttributes = function(propList){
  var vals = propList.vals ? propList.vals : false;
  if (!vals) {
    return new nodes.Expression();
  }
  var omitList = ['before', 'after', 'applyTo', 'selector', 'state', 'element', 'isRoot', 'hover', 'active', 'visited', 'focus', 'link', 'customState', 'anim', 'animation', 'transition', 'override', 'cache', '$$', 'import', 'duration', 'delay', 'ease', 'property', 'root', 'customeElement', 'option', 'globalOption', 'addClass', 'component', 'pseudo', 'error', 'type'];
  vals = _.omit(vals, omitList);
  return utils.coerceObject(vals, true); 
};


exports.isAllObjs = function(data) {
  var omitList = ['option', 'global', 'shorthand', 'timeline'];
  debugger
  // _.rest(arguments);
  if (omitList.length) {
    debugger
  }
  data = data.vals ? data.vals : false;
  return data ? _.every(data, function (val) {
    return _.every(val.nodes, Object);
  }) : false;
};

/**
 * formates stylus obj to js obj to work with
 */
var toObject = function (obj) {
  return _.reduce(obj.vals, function (res, n, key) {
    var nodeName = n.first.nodeName;
    if (nodeName === 'object') {
      res[key] = toObject(n.first);
    }else if (nodeName === 'rgba'){
      res[key] = n.first.toString();
    }else if (nodeName === 'unit') {
      res[key] = n.first.toString();
    }else if (nodeName === 'boolean') {
      res[key] = n.first.val;
    }else{
      debugger;
      console.log('!!!!!!!!!!!!!!!!!!!!!!');
      console.log(nodeName);
      res[key] = n.first.val;
    }
    return res;
  }, {});
};

exports.toObject = toObject;