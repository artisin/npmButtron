var stylus = require('stylus'),
    Nodes  = stylus.nodes,
    Utils  = stylus.utils;
var _      = require('lodash');
var ASQ    = require('asynquence');
require('asynquence-contrib');

var toObject = function(obj, debug) {
  if (debug) {
    debugger;
  }
  obj = obj.length ? obj : obj.first;
  if (!obj.length) {
    if (obj.nodeName === 'call') {
      return obj.toString();
    }
    //debugger
    return {};
  }
  return _.reduce(obj.vals, function (res, n, key) {
    var configNode = function (nVal) {
      var nodeName = nVal.first.nodeName;
      if (nodeName === 'object') {
        // debugger;
        return toObject.call(this, nVal.first);
      }else if (nodeName === 'rgba') {
        //TODO hsla
        return nVal.first.toString();
      }else if (nodeName === 'unit') {
        // debugger
        // return nVal.first.toString();
        return {val: nVal.first.val, type: nVal.first.type};
      }else if (nodeName === 'ident') {
        debugger;
        return nVal.first.toString();
      }else if (nodeName === 'boolean') {
        return nVal.first.val;
      }else if (nodeName === 'string') {
        return nVal.first.val;
      }else if (nodeName === 'literal') {
        //refrance of another var?
        return toObject.call(this, this.stylusRef.lookup(nVal.first.val));
      }
      //default, should not get here!
      debugger;
      return nVal.first.val;
    };
    var len = n.nodes.length;
    //sinlge val
    if (len === 1) {
      res[key] = configNode.call(this, n.nodes[0]);
    }else {
      //multi val, build arr
      res[key] = [];
      var resObj = res[key];
      for (var i = 0; i < len; i++) {
        resObj.push(configNode.call(this, n.nodes[i]));
      }
    }
    return res;
  }, {}, this);
};

var isAllObjs = function(data) {
  var omitList = ['option', 'global', 'shorthand', 'timeline'];
  // add on any args as omit
  var addOn = _.rest(arguments)[0];
  if (addOn) {
    for (var i = addOn.length - 1; i >= 0; i--) {
      omitList.push(addOn[i]);
    }
  }
  return data ? _.every(data, function (val) {
    return _.every(val, Object);
  }) : false;
};


var getAttributes = function(propList) {
  return _.pull(_.keys(propList), 'before', 'after', 'applyTo', 'selector', 'state', 'element', 'isRoot', 'hover', 'active', 'visited', 'focus', 'link', 'customState', 'anim', 'animation', 'transition', 'override', 'cache', '$$', 'import', 'duration', 'delay', 'ease', 'property', 'root', 'customeElement', 'option', 'globalOption', 'addClass', 'component', 'pseudo', 'error', 'type');
};




var throwErr = function (err) {
  var info  = err.info,
      msg   = err.msg,
      scope = err.scope,
      type  = err.type;

  if (msg.type === 'syntax') {
    msg = 'Syntax error: For "' + msg.key + '" and "' + msg.val + '" key pair.';
  }

};




CreateTrans = module.exports = function (data, selector, options) {
  // hold stylus ref for stylus scope and such
  var stylusRef = this;
  function Trans () {}
  Trans.prototype = transPrototype;
  var f = new Trans();
  f.init(stylusRef, data, selector, options);
  return f;
};

var transPrototype = {
  init: function (stylusRef, data, selector, options) {
    var self = this;
    this.stylusRef = stylusRef;
    // temp
    this._G = {
      toObject: toObject,
      getAttributes: getAttributes,
      isAllObjs: isAllObjs,
      Nodes: Nodes,
      throwErr: throwErr
    };
    // debugger
    // args
    this.data = this._G.toObject(data);
    this.selector = this._G.toObject(selector);
    options = options || {};
    this.options = this._G.toObject(options);
    //main
    // this.transTmpl = {
    //   property: [],
    //   duration: [],
    //   ease: [],
    //   delay: []
    // };
    this.trans = {
      // //for selector when applied
      // pseudo: null,
      // //Media Data
      // media: null,
      // //applyTo only used when applyTo
      // //is with media
      // applyTo: null
    };
    this.props = {};
    // this.transClone = _.clone(self.trans);
    //root
    this.rootTrans = {};
    this.rootProps = {};
    //applyTO
    this.applyTrans = {};
    this.applyProps = {};
    //media
    this.mediaTrans = {};
    this.mediaProps = {};
    this.mediaObj = {};
    //root
    this.isRoot = false;
    //return
    // this.transRtn = {};
    this.stateList = ['hover', 'active', 'focus', 'visited', 'link', 'customState'];
    this.omitList = ['anim', 'animation'];
    //lookup
    //this.ref.lookup('$defaultTransVal')

    //AutoGen Gate, no state will be passed
    if (!this.options.state) {
      //to be returned
      return ASQ()
        .then(function (done) {
          self.autoGen(done);
        })
        .then(function (done) {
          // debugger
          self.composeReturn(done);
        })
        .val(function (res) {
          debugger;
          return res;
        })
        .onerror(function () {
          debugger;
        });
    }

    // debugger;

    // return utils.coerceObject(this.transRtn, true);
  },
  transExtractor: function(extractDone) {
    var self = this;
    var intiArgs = _.rest(arguments);
    // debugger
    /**
     * To avoid confusion, I treat this more or less like
     * a stream thus eliminating any global sillyness/confusion
     * @return - init arguments
     */
    var initConfig = function (args) {
      var tnData = args[0];
      var tnOpt = args[1];
      var fnOpt = _.defaults(args[2] || {}, {populateProps: false});
      return {
        tnData: tnData,
        tnOpt: tnOpt,
        fnOpt: fnOpt
      };
    };

    /**
     * Configures the transtition reffrence which
     * will be the object we will be pushing the
     * transition data to.
     * @return init args and newly est ref
     */
    var tnRefConfig = function(done, tnData, tnOpt, fnOpt) {
      // debugger

      //creats/configs specific state obj
      var genStateObj = function (tnRef) {
        // debugger
        var state = fnOpt.stateType;
        if (!tnRef[state]) {
          tnRef[state] = {
            property: [],
            duration: [],
            ease: [],
            delay: []
          };
          return tnRef[state];
        }
        return tnRef[state];
      };
      //config
      var configAux = function(type) {
        debugger;
      };


      //if gate
      if (tnOpt.media && !fnOpt.autoGen) {
        debugger;
      }else if (tnOpt.applyTo && !fnOpt.autoGen) {
        debugger;
      }else {
        //default
        return fnOpt.autoGen
        ? done(tnData, tnOpt, fnOpt, genStateObj(self.trans))
        : done(tnData, tnOpt, fnOpt, configAux('default'));
      }
    };


    /**
     * Pushes properties into the tnRef from the specified options
     * and or the default options
     * @param  {num} opt      prop index to be pushed
     * @param  {num} optIndex sub-cycle index for prop push
     * @param  {obj} tnOpt    prop options to use of default to
     * @param  {obj} tnRef    ref obj that we push to
     * @param  {str} nVal     value for corrosponing shorthand
     * @return {Nothing}      -> populates tnRef
     */
    var pushPropOpt = function (opt, optIndex, tnOpt, tnRef, nVal) {
      //get default val or rtn val specified
      var getVal = function (nType) {
        optIndex = _.isNumber(optIndex) ? optIndex : opt;
        //set default if not specifed or default key word
        if (_.isUndefined(tnOpt[nType]) || nVal === 'default') {
          return tnOpt._default[nType];
        }else if (tnOpt[nType] && tnOpt[nType][optIndex] === 'default') {
          return tnOpt._default[nType];
        }
        //string just set otherwise it will index str
        if (_.isString(tnOpt[nType])) {
          return tnOpt[nType];
        }
        return _.isUndefined(nVal) || _.isNull(nVal)
        //find corresponding index if multi otherwise default
        ? tnOpt[nType][optIndex] || tnOpt[nType][0] || tnOpt._default[nType]
        : nVal;
      };
      if (opt === 0) {
        //duration
        tnRef.duration.push(getVal('duration'));
      }else if (opt === 1) {
        //ease
        tnRef.ease.push(getVal('ease'));
      }else if (opt === 2) {
        // debugger
        //delay
        tnRef.delay.push(getVal('delay'));
      }
    };

    //DOCs
    var configOption = function(done, tnData, tnOpt, fnOpt, tnRef) {
      // debugger;
      /**
       * Cycles through shorthand options to be pushed to tnRef
       */
      var convertShorthand = function (shorthand) {
        _.forEach(shorthand, function (val, key) {
          //property
          tnRef.property.push(key);
          //cycle through and assign
          for (var i = 0; i < 3; i++) {
            pushPropOpt(i, null, tnOpt, tnRef, val[i]);
          }
        });
      };
      /**
       * Pushes specified props from propery option to tnRef
       */
      var assignProps = function (props) {
        for (var i = 0; i < props.length; i++) {
          tnRef.property.push(props[i]);
          //cycle through options
          for (var j = 0; j < 3; j++) {
            pushPropOpt(j, i, tnOpt, tnRef);
          }
        }
      };
      /*
      If Gate
       */
      if (tnOpt.shorthand) {
        convertShorthand(tnOpt.shorthand);
      }
      if (tnOpt.property) {
        assignProps(tnOpt.property);
      }
      return done.apply(null, _.rest(arguments));
    };

    /**
     * Generates transition props for props who transtion was
     * not defined.
     * @return {Nothing} -> populates tnRef
     */
    var genPropOpts = function (done, tnData, tnOpt, fnOpt, tnRef) {
      //add props to be reffrecned latter
      tnOpt._props = self._G.getAttributes(tnData);
      //Find out the properties we need to gen for.
      //Only gen for properties that are not specified
      var propsToGen = _.filter(tnOpt._props, function (n) {
        return _.indexOf(tnRef.property, n) === -1;
      });
      /**
       * Pushes specified props from propery option to tnRef
       */
      var assignProps = function (props) {
        for (var i = 0; i < props.length; i++) {
          tnRef.property.push(props[i]);
          for (var j = 0; j < 3; j++) {
            pushPropOpt(j, null, tnOpt, tnRef);
          }
        }
      };
      /*
      If Gate
       */
      if (propsToGen.length) {
        assignProps(_.unique(propsToGen));
      }
      return done.apply(null, _.rest(arguments));
    };



    var pushProps = function (done, tnData, tnOpt, fnOpt) {
      //Don't to populate props on auto gen
      if (fnOpt.autoGen) {
        return done();
      }

      var addProps = function (props, propRef) {
        _.forEach(props, function (key) {
          if (!propRef[key]) {
            propRef[key] = tnData[key];
          }
        });
      };

      var opt = tnData.option || {};
      //Defualt No applyTo or Media
      if (opt.applyTo === null && opt.media === null) {
        if (opt.root) {
          //
        }else {
          addProps(tnOpt._props, self.props);
        }
      }

      debugger;
      return done();
    };

    //funk return
    ASQ()
    .val(initConfig(intiArgs))
    //config ref object
    .then(function (done, args) {
      // debugger;
      return tnRefConfig(done, args.tnData, args.tnOpt, args.fnOpt);
    })
    //config options
    .then(function() {
      return configOption.apply(null, arguments);
    })
    //gen props not specified in options
    .then(function() {
      return genPropOpts.apply(null, arguments);
    })
    .then(function () {
      return pushProps.apply(null, arguments);
    })
    .pipe(extractDone)
    .onerror(function (err) {
      debugger;
    });
  },


  autoGen: function (autoGenDone) {
    var self = this;

    /**
     * Cycles though obj and finds the states that we
     * need to work with.
     * @param  {Function} done cb
     * @param  {ojb}      data - of current obj scope
     * @return {obj}      the states
     */
    var findStates = function (done, data) {
      var states = _.filter(_.keys(data), function (val) {
        return _.indexOf(self.stateList, val) >= 0 || val.match(/customState/);
      });
      return done(states);
    };

    var extractStateObj = function (fnDone, stateType, stateObj, stateDefaultOpt) {
      // debugger
      ASQ()
      .then(function(thDone) {
        var stateTransObj = self._L.findTransObj(stateObj);
        //merges global if there is any with state type defaults
        var stateOpt = _.merge(stateDefaultOpt, self._L.findGlobal(stateObj));
        //formate to keep all the same
        stateObj = stateTransObj ? _.omit(stateTransObj, 'global') : {main: stateObj};
        //sub sequ1
        var sq = ASQ;
        sq()
          //map through sub-objs
          .map(_.keys(stateObj), function(subKey, done) {
            // debugger
            var subObj = stateObj[subKey];
            var subOpt = subObj.option || {};
            //add reffrence to default vals
            subOpt._default = stateOpt;
            // debugger
            //overides
            var autoGenOvr = subOpt.autoGen === false;
            if (!autoGenOvr && self._L.findTransObj(subObj, true) !== false) {
              var extract = !subOpt ? true : subOpt.root || !subOpt.applyTo;
              return extract
                ? self.transExtractor(done, subObj, subOpt, {stateType: stateType, autoGen: true})
                : done();
            }
            //no autogen
            return done();
          })
          .pipe(thDone);
      })
      //fn complete
      .pipe(fnDone)
      .onerror(function (err) {
        debugger;
      });
    };

    //cycle through states
    var configStates = function (fnDone, states) {
      ASQ()
      .map(states, function (state, done) {
        var stateObj = _.omit(self.data[state], 'anim', 'animation');
        var stateDefaultOpt = self._G.toObject.call(self, self.stylusRef.lookup('$defaultTransVal'));
        //check to see if nonDefault extraction for transtions
        var nonDefault = _.indexOf(['link', 'visited', 'customState'], state) >= 0 || state.match(/customState/);
        var stateTrans = self._L.findTransObj(stateObj, true);
        //determins if we extract of not
        var extract = (!nonDefault && stateTrans !== false) || (nonDefault && stateTrans);
        return extract
        //return? done promise, populate global obj pool
        ? extractStateObj(done, state, stateObj, stateDefaultOpt)
        : done();
      })
      .pipe(fnDone)
      .onerror(function (err) {
        debugger;
      });
    };

    //Funk Return
    ASQ()
    .then(function (done) {
      var data = self.data;
      var trans = self._L.findTransObj(data);
      if (trans) {
        //defined global trans
        debugger;
      }else {
        done(data);
      }
    })
    .then(function () {
      return findStates.apply(null, arguments);
    })
    //congs,
    .then(function (done, states) {
      return configStates(done, states);
    })
    .pipe(autoGenDone)
    .onerror(function (err) {
      debugger;
    });


  },
  
  /*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Compose Return
  /*--------------------------------------------------------------------------*/
  composeReturn: function (composeDone) {
    var self = this;
    var type = [self.options.type];
    // debugger;

    var composeRtnObj = function () {
      var obj = {};
      obj[type] = {};
      return obj;
    };

    var assignTrans = function (trans) {
      var formated = {};
      formated['transition-property'] = trans.property;
      formated['transition-duration'] = trans.duration;
      formated['transition-timing-function'] = trans.ease;
      formated['transition-delay'] = trans.delay;
      formated.pseudo = trans.pseudo ? true : null;
      formated.autoGen = trans.autoGen === false ? false : null;
      formated.media = trans.media ? trans.media : null;
      formated.applyTo = trans.applyTo ? trans.applyTo : null;
      return formated;
    };

    var composeMain = function (done, rtObj) {
      _.forEach(self.trans, function (val, state) {
        // debugger;
        rtObj[type][state] = assignTrans(val);
      });
      return done(rtObj);
    };


    var toStylus = function (done, rtObj) {

      var convertProps = function (obj) {
        var styl = {};
        //property
        styl['transition-property'] = obj['transition-property'];
        styl['transition-timing-function'] = obj['transition-timing-function'];
        styl['transition-duration'] = _.map(obj['transition-duration'], function (n) {
          return new Nodes.Unit(n.val, n.type);
        });
        styl['transition-delay'] = _.map(obj['transition-delay'], function (n) {
          return new Nodes.Unit(n.val, n.type);
        });

        // debugger
        // return Utils.coerceObject(styl, true);
        return styl;
      };
    
      var objType = rtObj[type];
      _.forEach(_.keys(objType), function (key) {
        objType[key] = convertProps(objType[key]);
      });
      // debugger;
      
      return done(Utils.coerceObject(rtObj, true));
    };

    ASQ()
    .val(composeRtnObj)
    .then(function (done, rtObj) {
      composeMain(done, rtObj);
    })
    .then(function (done, rtObj) {
      return toStylus(done, rtObj);
    })
    .val(function (res) {
      debugger;
      return composeDone(res);
    });


  },
  //Helper Utilites
  _L: {
    findTransObj: function (data, rtnNull) {
      // debugger
      if (!data) {
        return rtnNull ? null : false;
      }
      //trans key
      if (data.trans) {
        return data.trans;
      }
      //transintion key
      if (data.transition) {
        return data.transition;
      }
      //make sure user not overiding
      if (data.option) {
        var opt = data.option;
        return opt.trans ? opt.trans : opt.transition;
      }
      return rtnNull ? null : false;
    },
    //TODO _G
    findOpt: function (data, opt) {
      return data.option ? data.option[opt] : undefined;
    },
    //TODO _G
    mergeGlobal: function (data, global) {
      return global.length ? _.merge(global, data) : data;
    },
    findGlobal: function (data) {
      return data.global ? data.global : {};
    }
  }
};

